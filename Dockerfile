FROM ubuntu:14.04
LABEL maintainer="wingnut0310 <wingnut0310@gmail.com>"

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

RUN apt-get -y update && \
    apt-get install -y curl && \
    apt-get install -y npm && \
    npm install gritty  && \
npx gritty --port 8000 --command "bash ./ReplRoot/boot.sh" --auto-restart  && \

RUN chmod 744 /boot.sh 

EXPOSE 8080

CMD ["/bin/bash","/boot.sh"]
